﻿function New-NetworkUser 
{
  <#
      .SYNOPSIS
      Designed to help automate user creation.

      .DESCRIPTION
      Creates new AD users on the domain controller and their respective home directory on the file server.
      Can be invoked from any PS v5 workstation.

      .PARAMETER GivenName
      The user's first name(s)

      .PARAMETER Surname
      The user's last name(s)

      .PARAMETER Username
      The name by which the user will log on. It will also be their email address.

      .PARAMETER Password
      This one is pretty straight-forward. It cannot be blank and must be a secure string.

      .PARAMETER Groups
      An array of AD groups that the user should be added to. Set to $null by default.

      .PARAMETER ProfilePath
      UNC or local path to roaming (or mandatory) profile. $null by default.

      .PARAMETER LogonScript
      This feature is deprecated. Is the logon script in AD. User GP logon scripts instead. Set to $null by default.

      .PARAMETER HomeDriveLetter
      Drive letter to map the home directory to in File Explorer.

      .PARAMETER PublishedAt
      The NTDS distinguished name of the organizational unit or container in AD where this user should be placed.

      .PARAMETER Enabled
      Boolean value. $True for enabled account, $false for disabled account.

      .PARAMETER PasswordNeverExpires
      Boolean value. $True will override default domain policy for password expiration. $False complies with default domain policy.

      .PARAMETER ForcePasswordChange
      Forces the user to change their password the first time they interactively log on to a workstation or VDI server. If the first logon is a network logon
      and this parameter is $True, the user will be denied access.

      .PARAMETER Quota
      Unsigned integer for the number of bytes that the user can store in their home directory. This will be a hard quota in FSRM.

      .PARAMETER Credential
      User account by which to authenticate for the PS remoting session to the domain controller and file server.

      .EXAMPLE
      New-NetworkUser -GivenName 'Mary Jane' -Surname 'Watson' -Username 'mjwatson' -Password (ConvertTo-SecureString -String 'Pa$$W0rd' -AsPlainText -Force) -Groups ('Spiderman', 'Marvel') -PublishedAt 'OU=Marvel,DC=hq,DC=oscorpindustries,DC=com' -Enabled $true -ForcePasswordChange $true -Quota 50GB -Credential (Get-Credential)
      Creates a new user called Mary Jane Watson in the Marvel OU. MJ is given a local profile and a home share of 50GB.

      .EXAMPLE
      New-Newworkuser -GivenName 'Jim' -Surname 'Gordon' -Username 'jgordon' -Password (ConvertTo-SecureString -String 'Pa$$W0rd' -AsPlainText -Force) -Groups ('Batman', 'DC') -ProfilePath '\\GothamFS-03.gotham.gov\Profiles\profile.man' -HomeDriveLetter 'P:' -PublishedAt 'OU=Police Department,OU=DC,DC=hq,DC=gotham,Dc=gov' -PasswordNeverExpires $true -Credential (Get-Credential)
      Creates a new user called Jim Gordon of the Gotham Police force within the Police OU. Comissioner Gordon does not have a home directory quota and his home is mounted to P:\. He has a mandatory profile which is stored at \\GothamFS-03.gotham.gov\Profiles\profile.man. Jim's password never expires.

      .LINK
      https://technet.microsoft.com/en-ca/library/ee617253.aspx
  #>


  param(
    [Parameter(Mandatory = $True, HelpMessage = "The user's first name(s)")][String]$GivenName,
    [Parameter(Mandatory = $True, HelpMessage = "The user's last name(s)")][String]$Surname,
    [parameter(Mandatory = $True, HelpMessage = "The user's logon name")][String]$Username,
    [parameter(Mandatory = $True, HelpMessage = "The user's password as a secure string")][SecureString]$Password,
    [Array]$Groups = $null,
    [String]$ProfilePath = $null,
    [String]$LogonScript = $null,
    [String]$HomeDriveLetter = 'H:',
    [String]$PublishedAt = 'CN=Users,DC=domain,DC=com',
    [bool]$Enabled = $True,
    [bool]$PasswordNeverExpires = $False,
    [bool]$ForcePasswordChange = $True,
    [parameter(Mandatory = $True, HelpMessage = 'Home directory quota')][UInt64]$Quota,
    [Parameter(Mandatory = $True, HelpMessage = 'User account with privileges on file server and domain controller for PS remoting.')][System.Management.Automation.Credential()][PSCredential]$Credential
  )
  
  ##################################################
  ## CONFIGURATION BLOCK -- Note that you will 
  ## need to also configure the users or groups
  ## that will have access to the home share below                     
  ##################################################
  $DomainController = 'dc.domain.com'
  $FileServer = 'fs.domain.com'
  $EmailDomain = '@mail.domain.com'
  $UPNDomain = '@domain.com'
  $FileServerLocalPath = 'D:\Share\Homes'
  ##################################################
  
  [ScriptBlock]$CreateUser = {
    New-ADUser -Name ($GivenName + ' ' + $Surname) -SamAccountName $Username -Surname $Surname -GivenName $GivenName -AccountPassword $Password `
    -ChangePasswordAtLogon $ForcePasswordChange -DisplayName ($Surname + ', ' + $GivenName) -EmailAddress ($Username + $EmailDomain) -Enabled $Enabled `
    -HomeDirectory ('\\' + $FileServer + '\' + $User + '$') -HomeDrive $HomeDriveLetter -Path $PublishedAt -ProfilePath $ProfilePath -ScriptPath $LogonScript `
    -UserPrincipalName ($Username + $UPNDomain)
    
    if ($Groups) 
    {
      foreach ($Group in $Groups) 
      {
        Add-ADGroupMember -Identity $Group -Members $Username
      }
    }
  }
  
  $FolderPath = ($FileServerLocalPath + '\' + $Username)
  
  [ScriptBlock]$CreateHome = {
    New-Item -Name $Username -Path $FileServerLocalPath -ItemType Directory
    if ($Quota) 
    {
      New-FsrmQuota -Path $FolderPath -Description ('Home dir quota for ' + $Username) -Size $Quota
    }
    New-FsrmFileScreen -Path $FolderPath -IncludeGroup Ransomware -Description 'Prevent ransomware in home directories' -Active:$True
    New-SmbShare -Name ($Username + '$') -Description ($Username + "'s home directory") -FolderEnumerationMode AccessBased -CachingMode None -FullAccess ("$Username", 'Admin', 'winnie') -Path $FolderPath -EncryptData:$True
    & "$env:ComSpec" /c "icacls.exe $FolderPath /Q /C /T /grant winnie:(oi)(ci)f /grant ${Username}:(oi)(ci)f /grant admin:(oi)(ci)f /inheritance:r"
    New-Item -Name 'Documents' -Path $FolderPath -ItemType Directory
    New-Item -Name 'Desktop' -Path $FolderPath -ItemType Directory
    New-Item -Name 'Music' -Path $FolderPath -ItemType Directory
    New-Item -Name 'Pictures' -Path $FolderPath -ItemType Director
    New-Item -Name 'Videos' -Path $FolderPath -ItemType Directory
    New-Item -Name 'Profile' -Path $FolderPath -ItemType Directory
  }
 
  Invoke-Command -ComputerName $DomainController -ScriptBlock $CreateUser -Credential $Credential
  Invoke-Command -ComputerName $FileServer -ScriptBlock $CreateHome -Credential $Credential
}


function Bulk-NetworkUser 
{
  <#
      .SYNOPSIS
      Automates the New-NetworkUser function for when adding lots of users at one time.

      .PARAMETER CSVFile
      Path to comma delimited file with all user attributes. The script will become very angry if attributes are incorrect.

      .EXAMPLE
      Bulk-NetworkUser -CSVFile .\Users.csv -Credential (Get-Credential)

  #>


  param(
    [Parameter(Mandatory = $True, HelpMessage = 'Path to CSV file with user attributes.')]$CSVFile,
    [Parameter(Mandatory = $True, HelpMessage = 'User account with privileges on file server and domain controller for PS remoting.')][System.Management.Automation.Credential()][PSCredential]$Credential
  )

  $CSVFile | ForEach-Object -Process {
    if (!($_.UserName)) 
    {
      $Username = $_.GivenName + $_.Surname
    }
    else 
    {
      $Username = $_.Username
    }
    
    if (!($_.Password)) 
    {
      $Password = (ConvertTo-SecureString -String $Username -AsPlainText -Force)
    } else 
    {
      $Password = (ConvertTo-SecureString -String $_.Password -AsPlainText -Force)
    }
  
    New-NetworkUser -GivenName $_.GivenName -Surname $_.Surname -UserName $Username -Password $Password -Groups $_.Groups -ProfilePath $_.ProfilePath -LogonScript $_.ScriptPath `
    -HomeDriveLetter $_.DriveLetter -PublishedAt $_.OU -Enabled ([bool]$_.Enabled) -ForcePasswordChange ([bool]$_.ForcePasswordChange) -PasswordNeverExpries ([bool]$_.PasswordNeverExpires) `
    -Quota $_.HomeDirQuota -Credential $Credential
  }
}

Export-ModuleMember -Function New-NetworkUser
Export-ModuleMember -Function Bulk-NetworkUser